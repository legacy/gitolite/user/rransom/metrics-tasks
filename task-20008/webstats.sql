-- Copyright 2016 The Tor Project
-- See LICENSE for licensing information

CREATE TYPE method AS ENUM ('GET', 'HEAD');

CREATE TABLE files (
  file_id serial PRIMARY KEY,
  server character varying(32) NOT NULL,
  site character varying(128) NOT NULL,
  log_date date NOT NULL,
  UNIQUE (server, site, log_date)
);

CREATE TABLE resources (
  resource_id serial PRIMARY KEY,
  resource_string character varying(2048) UNIQUE NOT NULL
);

CREATE TABLE requests (
  file_id integer REFERENCES files (file_id) NOT NULL,
  method method NOT NULL,
  resource_id integer REFERENCES resources (resource_id) NOT NULL,
  response_code smallint NOT NULL,
  count integer NOT NULL,
  total_bytes_sent bigint NOT NULL,
  UNIQUE (file_id, method, resource_id, response_code)
);

